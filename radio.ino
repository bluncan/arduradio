#include <Wire.h>
#include<RDA5807M.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Rotary.h>

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 32
#define OLED_RESET -1

#define ROTARY_BTN 4
#define ROTARY_CLK 6
#define ROTARY_DT 5

#define BAND RADIO_BAND_FM
float frequency = 88.80f;
#define MIN_FREQUENCY 76.0
#define MAX_FREQUENCY 108.0

int volume = 5;
#define MAX_VOLUME 15
#define MIN_VOLUME 0

RDA5807M radio;
Rotary rotary = Rotary(ROTARY_DT, ROTARY_CLK);
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
int mode = 0;
bool isMuted = false;

int buttonState;
int lastButtonState = HIGH;
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

void setup()
{
  Serial.begin(9600);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) 
  {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  
  display.clearDisplay();
  display.setCursor(0, 0);
  display.write("   Radio");
  display.display();
  
  radio.init();
  radio.setBandFrequency(BAND, frequency * 100);
  radio.setVolume(volume);
  radio.setMono(false);
  radio.setMute(false);
  
  rotary.begin(true);
  pinMode(ROTARY_BTN, INPUT_PULLUP);
}  

  
void updateDisplay()
{
  display.clearDisplay();
  display.setCursor(0, 0);
  if (mode == 0)
  {
    display.print("   Volum\n     ");
    display.println(volume);
  }
  else
  {
    display.print(" Frecventa\n   ");
    display.println(frequency);
  }
  display.display();
}

void loop()
{
  unsigned char result = rotary.process();
  if (result) 
  {
    if (result == DIR_CW)
    {
      if (mode == 0 && volume < MAX_VOLUME) 
      {
        volume++;
      }
      else if (mode == 1 && frequency < MAX_FREQUENCY)
      {
        frequency += 0.1f;
      }
    }
    else
    {
      if (mode == 0 && volume > MIN_VOLUME)
      {
        volume--;
      }
      else if (mode == 1 && frequency > MIN_FREQUENCY)
      {
        frequency -= 0.1f;
      }
    }

    if (mode == 0)
    {
      if (volume == 0)
      {
        radio.setMute(true);
        isMuted = true;
      }
      else if (isMuted == true)
      {
        radio.setMute(false);
        isMuted = false;
      }
      radio.setVolume(volume);
    }
    else
    {
      radio.setBandFrequency( BAND, frequency * 100 );
    }
    Serial.print("volume: "); Serial.println(volume);
    Serial.print("freq: "); Serial.println(frequency);
    updateDisplay();
  }
    
  int reading = digitalRead(ROTARY_BTN);

  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (reading != buttonState) {
      buttonState = reading;
      if (buttonState == LOW) {
        mode ^= 1;
        updateDisplay();
        Serial.print("mode: ");
        Serial.println(mode);
      }
    }
  }

  lastButtonState = reading;
}
